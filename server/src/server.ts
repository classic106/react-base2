import express from "express";
import http from "http";
import cors from "cors";
import { PORT } from "./config";
import routes from "./routes";

const corsOptions = {
  origin: "http://localhost:3000",
  methods: ["GET", "POST", "DELETE", "PUT", "PATCH"],
};

const app = express();
const httpServer = new http.Server(app);

app.use(cors(corsOptions));
app.use(express.json());

routes(app);

app.get("*", (req, res) => res.redirect("/login"));

httpServer.listen(PORT, () => console.log(`Listen server on port ${PORT}`));

export default { app, httpServer };
