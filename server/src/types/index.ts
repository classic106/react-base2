interface IUser {
  id: string;
  name: string;
  email: string;
  avatar: string;
  isAdmin?: boolean;
}

interface IMessage {
  id: string;
  userId: string;
  avatar: string;
  user: string;
  text: string;
  createdAt: string;
  editedAt?: string;
}

export type { IUser, IMessage };
