import { IUser, IMessage } from "../types";

const fs = require("fs");
const path = require("path");

function getInf(inf: string) {
  try {
    const infPath = path.resolve(__dirname, `../data/${inf}.ts`);
    const data = fs.readFileSync(infPath, "utf8") as string;
    return JSON.parse(data) as IUser | IUser[] | IMessage;
  } catch (err) {
    return null;
  }
}

function getAllMessages() {
  return getInf("messages") as IMessage[] | null;
}

function getAllUsers() {
  return getInf("users") as IUser[] | null;
}

function getMessage(id: string) {
  const messages = getAllMessages();
  if (messages) {
    return messages.filter((message) => message.id === id)[0];
  }
  return null;
}

function getUser(id: string) {
  const users = getAllUsers();
  if (users) {
    return users.filter((user) => user.id === id)[0];
  }
  return null;
}

function writeUsers(users: IUser[]) {
  fs.writeFile(
    path.resolve(__dirname, "../data/users.ts"),
    JSON.stringify(users),
    "utf8",
    (err: Error) => {
      if (err) {
        return err;
      }
      return true;
    }
  );
}

function writeMessages(messages: IMessage[]) {
  fs.writeFileSync(
    path.resolve(__dirname, "../data/messages.ts"),
    JSON.stringify(messages),
    "utf8",
    (err: Error) => {
      if (err) {
        console.log(err);
        return err;
      }
      return true;
    }
  );
}

export {
  getAllMessages,
  getAllUsers,
  getMessage,
  getUser,
  writeUsers,
  writeMessages,
};
