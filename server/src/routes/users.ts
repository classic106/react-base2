import { Router } from "express";
import { getAllUsers, getUser, writeUsers } from "../helpers";

const router = Router();

router.get("/:currentUserId", (req, res) => {
  const { currentUserId } = req.params;
  const { userId } = req.query;

  const currentUser = getUser(currentUserId);

  if (userId && currentUser) {
    const user = getUser(userId as string);
    res.send(user);
    return;
  }

  if (currentUser) {
    const users = getAllUsers();
    if (users) {
      const filtertdUsers = users.filter((user) => user.id !== currentUserId);
      res.send(filtertdUsers);
      return;
    }
  }
  res.status(403).send({ message: "Access denied!!!" });
});

router.post("/:currentUserId", (req, res) => {
  const { currentUserId } = req.params;
  const user = req.body;

  const allUsers = getAllUsers();
  const currentUser = getUser(currentUserId);

  if (allUsers && currentUser?.isAdmin) {
    const isExistUser = allUsers.filter(
      (u) => u.name === user.name || u.email === user.email
    );

    if (isExistUser) {
      res.status(400).send({ message: "User is exist!!!" });
      return;
    }

    try {
      user.id = Date.now() + "";
      allUsers.push(user);
      writeUsers(allUsers);
      res.send({ user });
      return;
    } catch (err) {
      res.status(400).send(err);
      return;
    }
  }
  res.status(403).send({ message: "Access denied!!!" });
});

router.put("/:currentUserId", (req, res) => {
  const { currentUserId } = req.params;
  const user = req.body;
  const allUsers = getAllUsers();
  const currentUser = getUser(currentUserId);

  if (allUsers && currentUser?.isAdmin) {
    try {
      const index = allUsers.findIndex((u) => u.id === user.id);
      allUsers[index] = { ...allUsers[index], ...user };
      writeUsers(allUsers);
    } catch (err) {
      res.status(400).send(err);
      return;
    }
    res.send(user);
    return;
  }

  res.status(403).send({ message: "Access denied!!!" });
});

router.delete("/:currentUserId", (req, res) => {
  const { currentUserId } = req.params;
  const { userId } = req.query;

  const allUsers = getAllUsers();
  const currentUser = getUser(currentUserId);

  if (allUsers && currentUser?.isAdmin) {
    try {
      const index = allUsers.findIndex((u) => u.id === userId);
      if (index !== -1) {
        allUsers.splice(index, 1);
        writeUsers(allUsers);
        res.send({ userId });
        return;
      }

      res.status(400).send({ message: "User not found!!!" });
      return;
    } catch (err) {
      res.status(404).send(err);
      return;
    }
  }
  res.status(403).send({ message: "Access denied!!!" });
});

export default router;
