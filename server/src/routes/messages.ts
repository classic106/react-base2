import { Router } from "express";
import { getAllMessages, getMessage, getUser, writeMessages } from "../helpers";
import { IMessage } from "../types";

const router = Router();

router.get("/:currentUserId", (req, res) => {
  const { currentUserId } = req.params;
  const { messageId } = req.query;

  const currentUser = getUser(currentUserId);

  if (currentUser) {
    if (messageId) {
      const result = getMessage(messageId as string);
      res.send(result);
      return;
    } else {
      const messages = getAllMessages();
      res.send(messages);
      return;
    }
  }
  res.status(403).send({ message: "Access denied!!!" });
});

router.post("/add/:currentUserId", (req, res) => {
  const { currentUserId } = req.params;
  const { text } = req.body;

  const currentUser = getUser(currentUserId);

  if (currentUser) {
    try {
      const messages = getAllMessages();
      if (messages) {
        const newMessage: IMessage = {
          id: Date.now().toString(),
          userId: currentUserId,
          avatar: currentUser.avatar,
          user: currentUser.name,
          text: text,
          createdAt: new Date(Date.now()).toISOString(),
        };

        messages.push(newMessage);
        writeMessages(messages);

        res.send(newMessage);
        return;
      }
    } catch (err) {
      res.send(err);
      return;
    }
  }
  res.status(403).send({ message: "Access denied!!!" });
});

router.patch("/:currentUserId", (req, res) => {
  const { currentUserId } = req.params;
  const { messageId, text } = req.body;

  const currentUser = getUser(currentUserId);

  if (currentUser) {
    try {
      const messages = getAllMessages();
      if (messages) {
        const index = messages.findIndex(
          (message) =>
            message.id === messageId && message.userId === currentUserId
        );
        if (index !== -1) {
          const message = messages[index];
          message.text = text;
          message.editedAt = new Date(Date.now()).toISOString();
          messages[index] = message;

          writeMessages(messages);
          res.send(message);
          return;
        }
      }
    } catch (err) {
      res.send(err);
      return;
    }
  }
  res.status(403).send({ message: "Access denied!!!" });
});

router.delete("/:currentUserId", (req, res) => {
  const { currentUserId } = req.params;
  const { messageId } = req.query;

  const currentUser = getUser(currentUserId);

  if (currentUser) {
    try {
      const messages = getAllMessages();
      if (messages) {
        const index = messages.findIndex(
          (message) =>
            message.id === messageId && message.userId === currentUserId
        );

        if (index !== -1) {
          messages.splice(index, 1);
          writeMessages(messages);
          res.send({ messageId });
          return;
        }
      }
    } catch (err) {
      res.send(err);
      return;
    }
  }
  res.status(403).send({ message: "Access denied!!!" });
});

export default router;
