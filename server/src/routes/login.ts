import { Router } from "express";
import { getAllUsers, getUser } from "../helpers";

const router = Router();

router.get("/:userId", (req, res) => {
  const { userId } = req.params;

  const user = getUser(userId);

  if (userId && user) {
    res.send({ user });
    return;
  }
  res.status(404).send({ message: "User not found!!!" });
});

router.post("/", (req, res) => {
  const { name, email } = req.body;

  const users = getAllUsers();

  if (name && email && users) {
    const user = users.filter(
      (user) => user.name === name && user.email === email
    );

    if (user.length === 1) {
      res.send({ user: user[0] });
      return;
    }
  }
  res.status(404).send({ message: "User not found!!!" });
});

export default router;
