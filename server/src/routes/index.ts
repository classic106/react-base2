import { Express } from "express";
import login from "./login";
import users from "./users";
import messages from "./messages";

export default (app: Express) => {
  app.use("/login", login);
  app.use("/users", users);
  app.use("/messages", messages);
};
