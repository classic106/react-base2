import { store } from '../state/store';
import { actions } from '../state/userReducer';

const parseDate = (messageDate: string) => {
  if (messageDate) {
    const date = new Date(messageDate);
    const hour = date.getHours();
    const minutes = date.getMinutes().toString();
    return `${hour}:${minutes.length === 1 ? `0${minutes}` : minutes}`;
  }

  return null;
};

const logOutUser = () => {
  const { logOut } = actions;
  localStorage.removeItem('userId');
  store.dispatch(logOut());
};

const randomInteger = (min = 0, max: number) => {
  return Math.floor(min + Math.random() * (max - min));
};

export { parseDate, logOutUser, randomInteger };
