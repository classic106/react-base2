import { IMessage } from '../types';

async function getAllMessages(
  currentUserId: string
): Promise<IMessage[] | null> {
  if (currentUserId) {
    return fetch(`http://localhost:3002/messages/${currentUserId}`).then(
      (res) => res.json()
    );
  }
  return null;
}

async function addMessage(data: {
  text: string;
  currentUserId: string;
}): Promise<IMessage | null> {
  const { text, currentUserId } = data;

  if (currentUserId) {
    return fetch(`http://localhost:3002/messages/add/${currentUserId}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ text })
    }).then((res) => res.json());
  }
  return null;
}

async function updateMessage(data: {
  text: string;
  messageId: string;
  currentUserId: string;
}): Promise<IMessage | null> {
  const { text, currentUserId, messageId } = data;

  if (currentUserId) {
    return fetch(`http://localhost:3002/messages/${currentUserId}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ text, messageId })
    }).then((res) => res.json());
  }
  return null;
}

async function deleteMessage(data: {
  messageId: string;
  currentUserId: string;
}): Promise<{ messageId: string } | null> {
  const { messageId, currentUserId } = data;

  if (currentUserId) {
    return fetch(
      `http://localhost:3002/messages/${currentUserId}?messageId=${messageId}`,
      {
        method: 'DELETE'
      }
    ).then((res) => res.json());
  }
  return null;
}

export { addMessage, updateMessage, deleteMessage, getAllMessages };
