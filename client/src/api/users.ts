import { IUser } from '../types';
import { IAddUser } from '../types/stateTypes';

async function login(data: {
  name: string;
  email: string;
}): Promise<{ user: IUser; isAdmin: boolean; message?: string }> {
  return fetch('http://localhost:3002/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then((res) => res.json());
}

async function loginById(
  userId: string
): Promise<{ user: IUser; isAdmin: boolean; message?: string }> {
  return fetch(`http://localhost:3002/login/${userId}`).then((res) =>
    res.json()
  );
}

async function getAllUsers(currentUserId: string): Promise<IUser[] | null> {
  if (currentUserId) {
    return fetch(`http://localhost:3002/users/${currentUserId}`).then((res) =>
      res.json()
    );
  }
  return null;
}

async function addUser(
  data: IAddUser
): Promise<{ user: IUser; message: string } | null> {
  const { user, currentUserId } = data;

  if (currentUserId) {
    return fetch(`http://localhost:3002/users/${currentUserId}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
    }).then((res) => res.json());
  }
  return null;
}

async function deleteUser(data: {
  userId: string;
  currentUserId: string;
}): Promise<{ userId: string } | null> {
  const { userId, currentUserId } = data;

  if (currentUserId) {
    return fetch(
      `http://localhost:3002/users/${currentUserId}?userId=${userId}`,
      {
        method: 'DELETE'
      }
    ).then((res) => res.json());
  }
  return null;
}

async function updateUser(data: {
  currentUserId: string;
  user: IUser;
}): Promise<IUser | null> {
  const { user, currentUserId } = data;

  if (currentUserId) {
    return fetch(`http://localhost:3002/users/${currentUserId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
    }).then((res) => res.json());
  }
  return null;
}

export { login, addUser, deleteUser, updateUser, getAllUsers, loginById };
