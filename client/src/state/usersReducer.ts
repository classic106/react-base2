import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { IUser } from '../types';
import { IAddUser } from '../types/stateTypes';
import { getAllUsers, addUser, deleteUser, updateUser } from '../api/users';
import { actions as loadingActions } from './loading';
import { store } from './store';

const getUsers = createAsyncThunk('get_users', async (id: string) => {
  store.dispatch(loadingActions.startLoading());
  const response = await getAllUsers(id);
  store.dispatch(loadingActions.stopLoading());
  return response;
});

const addNewUser = createAsyncThunk(
  'user_add',
  async (data: IAddUser): Promise<{ user: IUser; message: string } | null> => {
    const response = await addUser(data);
    return response;
  }
);

const deleteOneUser = createAsyncThunk(
  'user_delete',
  async (data: {
    userId: string;
    currentUserId: string;
  }): Promise<{ userId: string } | null> => {
    const response = await deleteUser(data);
    return response;
  }
);

const updateOneUser = createAsyncThunk(
  'user_update',
  async (data: { currentUserId: string; user: IUser }): Promise<any> => {
    const responce = await updateUser(data);
    return responce;
  }
);

interface IUsersState {
  users: IUser[];
}

const initialState: IUsersState = { users: [] };

const counterSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    dropUsers(state) {
      state.users = [];
    }
  },
  extraReducers: (builder) => {
    builder.addCase(getUsers.fulfilled, (state, action) => {
      if (action.payload) {
        state.users = action.payload;
      }
    });
    builder.addCase(addNewUser.fulfilled, (state, action) => {
      if (action.payload) {
        const { message, user } = action.payload;
        if (message) {
          alert(message);
        } else if (user) {
          const newUsers = [...state.users];
          newUsers.push(user);
          state.users = newUsers;
        }
      }
    });
    builder.addCase(deleteOneUser.fulfilled, (state, action) => {
      if (action.payload) {
        const { userId } = action.payload;
        const newUsers = state.users.filter((user) => user.id !== userId);
        state.users = newUsers;
      }
    });
    builder.addCase(updateOneUser.fulfilled, (state, action) => {
      if (action.payload) {
        const { id } = action.payload;
        const newUsers = [...state.users];
        const index = newUsers.findIndex((user) => user.id === id);
        newUsers[index] = action.payload;
        state.users = newUsers;
      }
    });
  }
});

const { reducer, actions } = counterSlice;

export { reducer, actions, getUsers, addNewUser, deleteOneUser, updateOneUser };
