import { createSlice } from '@reduxjs/toolkit';

interface ILoadingData {
  loading: boolean;
}

const initialState: ILoadingData = { loading: false };

const counterSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    startLoading(state) {
      state.loading = true;
    },
    stopLoading(state) {
      state.loading = false;
    }
  }
});

const { reducer, actions } = counterSlice;

export { reducer, actions };
