import { createSlice } from '@reduxjs/toolkit';

interface IEditModel {
  open: boolean;
  userId: string | null;
}
const initialState: IEditModel = { open: false, userId: null };

const counterSlice = createSlice({
  name: 'editModal',
  initialState,
  reducers: {
    close(state) {
      state.open = false;
      state.userId = null;
    },
    open(state, action) {
      state.open = true;
      if (action.payload) {
        state.userId = action.payload;
      }
    }
  }
});

const { reducer, actions } = counterSlice;

export { reducer, actions };
