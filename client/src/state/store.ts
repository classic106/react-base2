import { configureStore } from '@reduxjs/toolkit';
import { reducer as users } from './usersReducer';
import { reducer as user } from './userReducer';
import { reducer as messages } from './messagesReducer';
import { reducer as editModal } from './editUserModalReducer';
import { reducer as loading } from './loading';

const store = configureStore({
  reducer: { user, users, messages, editModal, loading }
});

export { store };
export type RootState = ReturnType<typeof store.getState>;
