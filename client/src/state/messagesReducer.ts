import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { IMessage } from '../types';
import {
  getAllMessages,
  updateMessage,
  deleteMessage,
  addMessage
} from '../api/messages';
import { actions as loadingActions } from './loading';
import { store } from './store';

const getMessages = createAsyncThunk(
  'getMessages',
  async (currentUserId: string) => {
    store.dispatch(loadingActions.startLoading());
    const responce = await getAllMessages(currentUserId);
    store.dispatch(loadingActions.stopLoading());
    return responce;
  }
);

const addOneMessage = createAsyncThunk(
  'add_Message',
  async (data: { currentUserId: string; text: string }) => {
    const responce = await addMessage(data);
    return responce;
  }
);

const deleteOneMessage = createAsyncThunk(
  'delete_Message',
  async (data: { messageId: string; currentUserId: string }) => {
    const responce = await deleteMessage(data);
    return responce;
  }
);

const updateTextMessage = createAsyncThunk(
  'update_Message',
  async (data: { messageId: string; currentUserId: string; text: string }) => {
    const responce = await updateMessage(data);
    return responce;
  }
);

interface IMessagesState {
  messages: IMessage[];
  editMessage: IMessage | null;
}

const initialState: IMessagesState = { messages: [], editMessage: null };

const counterSlice = createSlice({
  name: 'messages',
  initialState,
  reducers: {
    dropAll(state) {
      state.messages = [];
      state.editMessage = null;
    },
    setEditMessage(state, action) {
      const message = state.messages.filter((m) => m.id === action.payload)[0];
      if (message) {
        state.editMessage = message;
      }
    },
    dropEditMessage(state) {
      state.editMessage = null;
    }
  },
  extraReducers: (builder) => {
    builder.addCase(getMessages.fulfilled, (state, action) => {
      if (action.payload) {
        state.messages = action.payload;
      }
    });
    builder.addCase(deleteOneMessage.fulfilled, (state, action) => {
      if (action.payload) {
        const { messageId } = action.payload;
        const newMessages = state.messages.filter(
          (message) => message.id !== messageId
        );
        state.messages = newMessages;
      }
    });
    builder.addCase(updateTextMessage.fulfilled, (state, action) => {
      if (action.payload) {
        const { id, text } = action.payload;

        const newMessages = [...state.messages];
        const index = newMessages.findIndex((m) => m.id === id);

        newMessages[index].text = text;
        state.messages = newMessages;
      }
    });
    builder.addCase(addOneMessage.fulfilled, (state, action) => {
      if (action.payload) {
        const newMessages = [...state.messages];
        newMessages.push(action.payload);
        state.messages = newMessages;
      }
    });
  }
});

const { reducer, actions } = counterSlice;

export {
  reducer,
  actions,
  getMessages,
  addOneMessage,
  deleteOneMessage,
  updateTextMessage
};
