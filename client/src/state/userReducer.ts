import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { IUserState } from '../types/stateTypes';
import { login, loginById } from '../api/users';

const loginUser = createAsyncThunk(
  'login',
  async (user: { name: string; email: string }) => {
    const response = await login(user);
    return response;
  }
);

const loginByUserId = createAsyncThunk(
  'loginByUserId',
  async (userId: string) => {
    const response = await loginById(userId);
    return response;
  }
);

const initialState: IUserState = { user: null, logError: null };

const counterSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    logOut(state) {
      state.user = null;
    },
    dropLogError(state) {
      state.logError = null;
    },
    setLogError(state, action) {
      state.logError = action.payload;
    }
  },
  extraReducers: (builder) => {
    builder.addCase(loginUser.fulfilled, (state, action) => {
      const { user, message } = action.payload;
      if (user) {
        localStorage.setItem('userId', user.id);
        state.user = user;
      } else if (message) {
        state.logError = message;
      }
    });
    builder.addCase(loginByUserId.fulfilled, (state, action) => {
      const { user, message } = action.payload;
      if (user) {
        state.user = user;
      } else if (message) {
        state.logError = message;
      }
    });
  }
});

const { reducer, actions } = counterSlice;

export { reducer, actions, loginUser, loginByUserId };
