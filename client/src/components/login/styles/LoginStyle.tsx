import styled from 'styled-components';

const LoginStyle = styled.div`
  display: flex;
  width: 100%;
  height: 100vh;
  background-color: #4a148c;
  justify-content: center;
  color: white;

  div {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 50px;

    input[type='text'] {
      background: none;
      border: none;
      color: white;
      text-align: center;
      font-size: 16px;
      padding: 10px;
    }

    input[type='text']::placeholder {
      color: lightgrey;
    }

    input[type='text']:focus-visible {
      outline: none !important;
      box-shadow: inset 0 0 8px #719ece;
    }

    button {
      border: none;
      color: white;
      background: none;
      padding: 4px;
      margin: 2px;
      width: 100px;
      margin-top: 20px;
      box-shadow: 0 0 10px #719ece;
    }

    button:hover {
      color: yellow;
      box-shadow: 0 0 10px yellow;
    }

    .access_button:hover {
      color: lightgreen;
      box-shadow: 0 0 10px lightgreen;
    }

    .login_error {
      background-color: red;
      border-radius: 30px;
      padding: 13px;
      min-height: 22px;
      width: max-content;
      color: yellow;
      margin-top: 20px;
    }
  }
`;

export { LoginStyle };
