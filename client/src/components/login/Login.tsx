import React, { useState, ChangeEvent, FC, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { LoginStyle } from './styles/LoginStyle';

import { loginByUserId } from '../../state/userReducer';
import { actions } from '../../state/userReducer';
import { IUserState } from '../../types/stateTypes';
import { store, RootState } from '../../state/store';
import { loginUser } from '../../state/userReducer';
import { getMessages } from '../../state/messagesReducer';
import { getUsers } from '../../state/usersReducer';

const Login: FC = () => {
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');

  const { logError } = useSelector((store: RootState) => store.user);
  const { dropLogError, setLogError } = actions;

  const userId = localStorage.getItem('userId');
  const navigate = useNavigate();

  useEffect(() => {
    const getUser = async () => {
      if (userId) {
        const { payload } = await store.dispatch(loginByUserId(userId));
        const { user } = payload as IUserState;

        if (user) {
          if (user.isAdmin) {
            store.dispatch(getUsers(user.id));
            navigate(`/users`);
          } else {
            store.dispatch(getMessages(user.id));
            navigate(`/`);
          }
        }
      }
    };
    getUser();
  });

  const onClick = async () => {
    if (name && email) {
      const { payload } = await store.dispatch(loginUser({ name, email }));
      const { user } = payload as IUserState;

      if (user) {
        const { id, isAdmin } = user;

        if (isAdmin) {
          store.dispatch(getUsers(id));
          navigate(`/users`);
        } else {
          store.dispatch(getMessages(id));
          navigate(`/`);
        }
      }
      return;
    }

    setTimeout(() => store.dispatch(dropLogError()), 3000);

    if (!name && !email) {
      store.dispatch(setLogError('Fields are empty'));
      return;
    }
    if (!name) {
      store.dispatch(setLogError('Login is empty'));
    } else if (!email) {
      store.dispatch(setLogError('Email is empty'));
    }
  };

  const changeLogin = (e: ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value);
    store.dispatch(dropLogError());
  };

  const changeEmail = (e: ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
    store.dispatch(dropLogError());
  };

  if (userId) {
    return <></>;
  }

  return (
    <LoginStyle>
      <div>
        <h2>Enter to chat</h2>
        <input
          type="text"
          placeholder="Enter name"
          value={name}
          onChange={changeLogin}
        />
        <input
          type="text"
          placeholder="Enter email"
          value={email}
          onChange={changeEmail}
        />
        <button onClick={onClick}>login</button>
        <div className="login_error" style={{ opacity: logError ? 1 : 0 }}>
          {logError}
        </div>
      </div>
    </LoginStyle>
  );
};

export { Login };
