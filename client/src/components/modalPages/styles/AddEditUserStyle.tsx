import styled from 'styled-components';

const AddEditUserStyle = styled.div`
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #0000004a;

  .edit_user_main {
    background-color: #7c43bd;
    color: white;
    padding: 45px;
    border-radius: 4px;

    h4 {
      display: flex;
      justify-content: center;
    }

    .edit_user_content_main {
      display: flex;
      align-items: center;
      margin-bottom: 30px;

      input[type='text'] {
        background: none;
        border: none;
        color: white;
        text-align: center;
        font-size: 16px;
        padding: 10px;
      }

      input[type='text']::placeholder {
        color: lightgrey;
      }

      input[type='text']:focus-visible {
        outline: none !important;
        box-shadow: inset 0 0 8px #719ece;
      }

      label {
        padding: 10px;
        position: relative;
      }

      label:hover::after {
        opacity: 1;
      }

      label::after {
        content: 'click to change';
        opacity: 0;
        width: max-content;
        height: 15px;
        top: -28px;
        position: absolute;
        color: yellow;
        left: 41px;
        background-color: grey;
        padding: 9px;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 11px;
        border-radius: 22px 0px 22px 0px;
      }

      .admin {
        color: lightgreen;
      }

      .not_admin {
        color: lightcoral;
      }
    }

    .edit_user_btns {
      display: flex;
      justify-content: flex-end;

      button {
        border: none;
        color: white;
        background: none;
        padding: 4px;
        margin: 2px;
        width: 100px;
      }

      button:hover {
        color: yellow;
        box-shadow: 0 0 10px #719ece;
      }

      .access_button:hover {
        color: lightgreen;
        box-shadow: 0 0 10px lightgreen;
      }
    }
  }
`;

export { AddEditUserStyle };
