import React, { useState, FC, ChangeEvent, useEffect, MouseEvent } from 'react';
import { useSelector } from 'react-redux';

import { AddEditUserStyle } from './styles/AddEditUserStyle';

import { RootState, store } from '../../state/store';
import { actions } from '../../state/editUserModalReducer';
import { addNewUser, updateOneUser } from '../../state/usersReducer';

const AddEditUser: FC = () => {
  const { userId } = useSelector((store: RootState) => store.editModal);
  const { users } = useSelector((store: RootState) => store.users);
  const { user } = useSelector((store: RootState) => store.user);

  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [isAdmin, setIsAdmin] = useState<boolean>(false);

  useEffect(() => {
    if (userId) {
      const user = users.filter((user) => user.id === userId)[0];
      if (user) {
        const { name, email, isAdmin } = user;
        setName(name);
        setEmail(email);
        setIsAdmin(!!isAdmin);
      }
    }
  }, [userId, users]);

  const onChange = () => setIsAdmin(!isAdmin);

  const sendEdit = () => {
    if (!name || !email) {
      if (!name) {
        alert('Name shouldn`t be empty!!!');
      }

      if (!email) {
        alert('Email shouldn`t be empty!!!');
      }
      return;
    }

    if (user) {
      if (userId) {
        const updateUser = { id: userId, name, email, isAdmin };
        store.dispatch(
          updateOneUser({ currentUserId: user.id, user: updateUser })
        );
      } else {
        const newUser = { name, email, isAdmin };
        store.dispatch(addNewUser({ currentUserId: user.id, user: newUser }));
      }
      store.dispatch(actions.close());
    }
  };

  const cancel = () => store.dispatch(actions.close());
  const onClose = () => store.dispatch(actions.close());
  const onClick = (e: MouseEvent<HTMLDivElement>) => e.stopPropagation();
  const onChangeName = (e: ChangeEvent<HTMLInputElement>) =>
    setName(e.target.value);
  const onChangeEmail = (e: ChangeEvent<HTMLInputElement>) =>
    setEmail(e.target.value);

  return (
    <AddEditUserStyle onClick={onClose}>
      <div onClick={onClick} className="edit_user_main">
        <div className="edit_user_content">
          <h4>{userId ? 'Edit' : 'Add'} user</h4>
          <div className="edit_user_content_main">
            <input
              type="text"
              name="name"
              value={name}
              onChange={onChangeName}
              placeholder="set user name"
            />
            <input
              type="text"
              name="email"
              value={email}
              onChange={onChangeEmail}
              placeholder="set user email"
            />
            <label
              className={isAdmin ? 'admin' : 'not_admin'}
              onClick={onChange}
            >
              admin
            </label>
          </div>
        </div>
        <div className="edit_user_btns">
          <button onClick={sendEdit} className="access_button">
            OK
          </button>
          <button onClick={cancel}>Cancel</button>
        </div>
      </div>
    </AddEditUserStyle>
  );
};

export { AddEditUser };
