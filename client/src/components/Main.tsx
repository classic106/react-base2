import React, { FC } from 'react';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { RootState } from '../state/store';
import { Chat } from './chat/Chat';
import { Login } from './login/Login';
import { AddEditUser } from './modalPages/AddEditUser';
import { EditUsers } from './users/EditUsers';

const Main: FC = () => {
  const { open } = useSelector((store: RootState) => store.editModal);

  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<Chat />} />
        <Route path="login" element={<Login />} />
        <Route path="users" element={<EditUsers />} />
        <Route path="*" element={<Chat />} />
      </Routes>
      {open && <AddEditUser />}
    </BrowserRouter>
  );
};

export { Main };
