import React, { FC, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { ChatStyle } from './styles/ChatStyle';

import { RootState } from '../../state/store';
import { randomInteger } from '../../helpers';

import { Header } from './Header';
import { MessagesList } from './MessagesList';
import { MessageInput } from './MessageInput ';

const Chat: FC = () => {
  const { messages } = useSelector((store: RootState) => store.messages);
  const { user } = useSelector((store: RootState) => store.user);

  const navigate = useNavigate();

  useEffect(() => {
    if (!user) {
      navigate(`/login`);
    }
  });

  return (
    <ChatStyle className="chat">
      <Header
        participantsCount={randomInteger(0, 100)}
        messagesCount={messages.length}
        lastMessgeDate={messages[messages.length - 1]?.createdAt}
      />
      <MessagesList messages={messages} />
      <MessageInput />
    </ChatStyle>
  );
};

export { Chat };
