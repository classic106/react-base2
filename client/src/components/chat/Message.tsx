import React, { FC } from 'react';

import { MessageStyle } from './styles/MessageStyle';
import { IMessage as MessageProps } from '../../types';

import { parseDate } from '../../helpers';

interface IMessage {
  message: MessageProps;
}

const Message: FC<IMessage> = ({ message }) => {
  let avatar = '';

  if (message.avatar) {
    avatar = message.avatar;
  } else {
    avatar =
      'https://t3.ftcdn.net/jpg/03/64/62/36/360_F_364623624_eTeYrOr8oM08nsPPEmV8gGb60E0MK5vp.jpg';
  }

  const onClick = () => {
    alert('You liked!!!');
  };

  return (
    <MessageStyle className="message">
      <div className="message_left_side">
        <div className="image_wrap">
          <img src={avatar} alt={message.user} />
        </div>
        <p className="messge_user">{message.user}</p>
      </div>
      <div className="left_main">
        <p className="message_text">{message.text}</p>
        <div className="left_main_bottom">
          <p className="message_time">{parseDate(message.createdAt)}</p>
          <button className="message-like button" onClick={onClick}>
            Like
          </button>
        </div>
      </div>
    </MessageStyle>
  );
};

export { Message };
