import React, { FC, useState, ChangeEvent } from 'react';
import { useSelector } from 'react-redux';

import { MessageInputStyle } from './styles/MessageInputStyle';
import { RootState } from '../../state/store';
import { store } from '../../state/store';
import { addOneMessage } from '../../state/messagesReducer';

import { EditMessage } from './EditMessage';

const MessageInput: FC = () => {
  const { editMessage } = useSelector((store: RootState) => store.messages);
  const { user } = useSelector((store: RootState) => store.user);

  const [messageText, setMessageText] = useState<string>('');

  const onChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    const { value } = e.target;
    setMessageText(value);
  };

  const onClick = () => {
    if (messageText && user) {
      store.dispatch(
        addOneMessage({ currentUserId: user.id, text: messageText })
      );
      setMessageText('');
    } else {
      alert('Empty field!!!');
    }
  };

  if (editMessage) {
    return (
      <MessageInputStyle className="message_input">
        <EditMessage />
      </MessageInputStyle>
    );
  }

  return (
    <MessageInputStyle className="message_input">
      <textarea
        onChange={onChange}
        className='message_input_text'
        placeholder="Enter your message..."
        value={messageText}
      ></textarea>
      <button className="message_input_button" onClick={onClick}>
        {'SEND'}
      </button>
    </MessageInputStyle>
  );
};

export { MessageInput };
