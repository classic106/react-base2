import styled from 'styled-components';

const HeaderStyle = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 100%;
  height: 100px;
  background-color: #4a148c;
  color: white;

  .header_title {
    padding: 20px;
  }

  .header_content {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    margin-right: 20px;

    .header_content_center {
      display: flex;
      justify-content: start;
      flex-grow: 3;
      padding: 20px 0;

      .header_users_count {
        margin-right: 10px;
      }
    }

    .header_last_message_date {
      text-align: end;
      padding: 20px 0;
      flex-grow: 1;
    }
  }

  .header_buttons {
    display: flex;
    flex-direction: column;
    width: 12%;

    button {
      border: none;
      color: white;
      background: none;
      padding: 4px;
      margin: 2px;
    }

    button:hover {
      color: yellow;
      box-shadow: 0 0 10px #719ece;
    }
  }
`;

export { HeaderStyle };
