import styled from 'styled-components';

const EditMessageStyle = styled.div`
  display: flex;
  width: 100%;

  .left_side {
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    color: white;
    margin: 0;

    p {
      display: flex;
      justify-content: center;
      margin: 0;
      padding: 4px;
    }

    textarea {
      resize: none;
      background: none;
      border: none;
      color: white;
      width: 99%;
    }

    textarea:focus,
    textarea.focus {
      outline: none !important;
      box-shadow: inset 0 0 8px #719ece;
    }
  }

  .right_side {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;

    button {
      background: none;
      border: none;
      color: white;
      font-size: 15px;
      padding: 15px;
      width: 100%;
    }

    .cancel_button:hover {
      color: yellow;
    }

    .access_button:hover {
      color: lightgreen;
    }
  }
`;

export { EditMessageStyle };
