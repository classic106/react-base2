import React, { FC, useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';

import { MessagesListStyle } from './styles/MessagesStyle';

import { IMessage } from '../../types';
import { RootState } from '../../state/store';

import { Message } from './Message';
import { Preloader } from '../preloader/Preloader';
import { OwnMessage } from './OwnMessage';

interface IMessages {
  messages: IMessage[];
}

const MessagesList: FC<IMessages> = ({ messages }) => {
  const { user } = useSelector((store: RootState) => store.user);
  const { loading } = useSelector((store: RootState) => store.loading);

  const messgesListRef = useRef<HTMLDivElement>(null);
  const { current } = messgesListRef;

  useEffect(() => {
    if (messgesListRef.current) {
      const height = messgesListRef.current.scrollHeight;
      messgesListRef.current.scrollTo(0, height);
    }
  }, [current]);

  if (loading) {
    return (
      <MessagesListStyle>
        <Preloader />
      </MessagesListStyle>
    );
  }

  return (
    <MessagesListStyle ref={messgesListRef}>
      <div className="wrap_message_list">
        {messages.map((message) => {
          if (message.userId === user?.id) {
            return <OwnMessage message={message} key={message.id} />;
          }
          return <Message message={message} key={message.id} />;
        })}
      </div>
    </MessagesListStyle>
  );
};

export { MessagesList };
