import React, { FC } from 'react';
import { useSelector } from 'react-redux';

import { OwnMessageStyle } from './styles/OwnMessageStyle';

import { store, RootState } from '../../state/store';
import { IMessage as MessageProps } from '../../types';
import { parseDate } from '../../helpers';
import { actions, deleteOneMessage } from '../../state/messagesReducer';

interface IMessage {
  message: MessageProps;
}

const OwnMessage: FC<IMessage> = ({ message }) => {
  const { user } = useSelector((store: RootState) => store.user);

  const onClick = () => store.dispatch(actions.setEditMessage(message.id));
  const onDelete = () => {
    if (user) {
      store.dispatch(
        deleteOneMessage({ messageId: message.id, currentUserId: user.id })
      );
    }
  };

  return (
    <OwnMessageStyle className="own_message">
      <p className="message_text">{message.text}</p>
      <div className="right_bottom">
        <p className="message_time">{parseDate(message.createdAt)}</p>
        <button className="message-edit button" onClick={onClick}>
          edit
        </button>
        <button className="message-delete button" onClick={onDelete}>
          delete
        </button>
      </div>
    </OwnMessageStyle>
  );
};

export { OwnMessage };
