import React, { FC } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { HeaderStyle } from './styles/HeaderStyle';

import { store, RootState } from '../../state/store';
import { parseDate, logOutUser } from '../../helpers';
import { getMessages } from '../../state/messagesReducer';

interface IHeader {
  participantsCount: number;
  messagesCount: number;
  lastMessgeDate: string | null;
}

const Header: FC<IHeader> = ({
  participantsCount,
  messagesCount,
  lastMessgeDate
}) => {
  const { user } = useSelector((store: RootState) => store.user);
  const navigate = useNavigate();

  const onLogOut = () => logOutUser();
  const onToUsers = () => {
    if (user) {
      store.dispatch(getMessages(user.id));
      navigate(`/users`);
    }
  };

  return (
    <HeaderStyle className="header">
      <div className="header_title">MyChat</div>
      <div className="header_content">
        <div className="header_content_center">
          <p className="header_users_count">{participantsCount} participants</p>
          <p className="header_messages_count">{messagesCount} messages</p>
        </div>
        <p className="header_last_message_date">
          last message at {parseDate(lastMessgeDate as string)}
        </p>
      </div>
      <div className='header_buttons'>
        {user?.isAdmin && <button onClick={onToUsers}>To Users</button>}
        <button onClick={onLogOut}>LOGOUT</button>
      </div>
    </HeaderStyle>
  );
};

export { Header };
