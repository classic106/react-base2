import React, { useState, FC, ChangeEvent } from 'react';
import { useSelector } from 'react-redux';

import { EditMessageStyle } from './styles/EditMessageStyle';

import { RootState, store } from '../../state/store';
import { actions, updateTextMessage } from '../../state/messagesReducer';

const EditMessage: FC = () => {
  const { editMessage } = useSelector((store: RootState) => store.messages);
  const { user } = useSelector((store: RootState) => store.user);

  const [text, setText] = useState<string>(editMessage ? editMessage.text : '');

  const onChange = (e: ChangeEvent<HTMLTextAreaElement>) =>
    setText(e.target.value);

  const sendEdit = () => {
    if (editMessage && user) {
      const body = { messageId: editMessage.id, currentUserId: user.id, text };
      store.dispatch(updateTextMessage(body));
    }
  };

  const cancel = () => store.dispatch(actions.dropEditMessage());

  return (
    <EditMessageStyle>
      <div className="left_side">
        <p>Edit Message</p>
        <textarea
          name="message"
          id="message"
          cols={30}
          rows={10}
          value={text}
          onChange={onChange}
        ></textarea>
      </div>
      <div className="right_side">
        <button onClick={cancel} className='cancel_button'>Cancel</button>
        <button onClick={sendEdit} className='access_button'>OK</button>
      </div>
    </EditMessageStyle>
  );
};

export { EditMessage };
