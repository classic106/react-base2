import React, { FC } from 'react';

import { PreloaderFullScreenStyle } from './styles/PreloaderFullScreenStyle';

import { Preloader } from './Preloader';

const PreloaderFullScreen: FC = () => {
  return (
    <PreloaderFullScreenStyle>
      <Preloader />
    </PreloaderFullScreenStyle>
  );
};

export { PreloaderFullScreen };
