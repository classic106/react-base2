import styled from 'styled-components';

const PreloaderFullScreenStyle = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justifycontent: center;
  background-color: #4a148c;
`;

export { PreloaderFullScreenStyle };
