import React, { useState, FC, ChangeEvent } from 'react';
import { IUser } from '../../types';

interface userProps {
  user: IUser;
}

const User: FC<userProps> = ({ user }) => {
  const [userName, setUserName] = useState<string>(user.name);
  const [userEmail, setUserEmail] = useState<string>(user.email);

  const editUser = () => {
    const newUser = {
      name: userName,
      email: userEmail,
      id: user.id
    };
    console.log(newUser);
  };
  const deleteUser = () => {
    //send to derver
  };
  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const inputValues = value.split(' ');

    if (inputValues.length !== 2) {
      alert('Wrong input values!!!');
      return;
    }
    if (inputValues.length === 2) {
      setUserName(inputValues[0]);
      setUserEmail(inputValues[1]);
    }
  };

  return (
    <div>
      <input
        type="text"
        value={`${userName} ${userEmail}`}
        onChange={onChange}
      />
      <button onClick={editUser}>Edit</button>
      <button onClick={deleteUser}>Delete</button>
    </div>
  );
};

export { User };
