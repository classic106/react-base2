import styled from 'styled-components';

const EditUserStyle = styled.div`
  display: flex;
  margin-bottom: 10px;
  position: relative;
  background-color: #4a148c;
  padding: 10px;

  .user_content {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 5px 5px 5px 20px;
    width: 100%;

    p {
      display: flex;
      justify-content: center;
      margin-right: 30px;
    }

    label {
      padding: 10px;
    }

    .admin {
      color: lightgreen;
    }

    .not_admin {
      color: lightcoral;
    }
  }

  .user_buttons {
    display: flex;
    justify-content: space-between;
    flex-direction: column;

    button {
      border: none;
      color: white;
      background: none;
      padding: 4px;
      margin: 2px;
      width: 100px;
    }

    button:hover {
      color: yellow;
      box-shadow: 0 0 10px #719ece;
    }

    .delete_user:hover {
      color: red;
      box-shadow: 0 0 10px red;
    }
  }
`;

export { EditUserStyle };
