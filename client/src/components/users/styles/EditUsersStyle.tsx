import styled from 'styled-components';

const EditUsersStyle = styled.div`
  width: 100%;
  height: calc(100vh - 100px);
  overflow-x: auto;
  paddin: 10px;
  background-color: #7c43bd;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: white;
  overflow-y: auto;

  .add_new_user_btn {
    border: none;
    color: white;
    background: none;
    padding: 15px;
    margin: 2px;
    font-weight: bold;
    margin-bottom: 20px;
  }

  .add_new_user_btn:hover {
    color: yellow;
    box-shadow: 0 0 10px #719ece;
  }
}
`;

export { EditUsersStyle };
