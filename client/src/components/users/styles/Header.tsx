import styled from 'styled-components';

const HeaderStyle = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 100%;
  height: 100px;
  background-color: #4a148c;
  color: white;

  button {
    border: none;
    color: white;
    background: none;
    padding: 10px 20px;
    margin: 2px;
  }

  button:hover {
    color: yellow;
    box-shadow: 0 0 10px #719ece;
  }
`;

export { HeaderStyle };
