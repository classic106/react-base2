import React, { FC } from 'react';
import { useSelector } from 'react-redux';

import { EditUserStyle } from './styles/EditUserStyle';

import { IUser } from '../../types';
import { store, RootState } from '../../state/store';
import { actions } from '../../state/editUserModalReducer';
import { deleteOneUser } from '../../state/usersReducer';

interface IEditUser {
  user: IUser;
}

const EditUser: FC<IEditUser> = ({ user }) => {
  const { name, id, email, isAdmin } = user;
  const currentUser = useSelector((store: RootState) => store.user);

  const onEdit = () => store.dispatch(actions.open(id));
  const onDelete = () => {
    if (currentUser.user) {
      const { id } = currentUser.user;
      store.dispatch(deleteOneUser({ currentUserId: id, userId: user.id }));
    }
  };

  return (
    <EditUserStyle>
      <div className="user_content">
        <p>
          {name} {email}
        </p>
        <label className={isAdmin ? 'admin' : 'not_admin'}>admin</label>
      </div>
      <div className="user_buttons">
        <button onClick={onEdit} className="edit_user">
          Edit
        </button>
        <button onClick={onDelete} className="delete_user">
          Delete
        </button>
      </div>
    </EditUserStyle>
  );
};

export { EditUser };
