import React, { FC } from 'react';
import { useSelector } from 'react-redux';

import { store, RootState } from '../../state/store';
import { actions } from '../../state/editUserModalReducer';

import { EditUser } from './EditUser';
import { Preloader } from '../preloader/Preloader';

const RenderUsers: FC = () => {
  const { users } = useSelector((store: RootState) => store.users);
  const { loading } = useSelector((store: RootState) => store.loading);

  const openModal = () => store.dispatch(actions.open(''));

  if (loading) {
    return <Preloader />;
  }

  return (
    <>
      <button onClick={openModal} className="add_new_user_btn">
        Add new user
      </button>
      <div className="users_list">
        {users.map((user) => (
          <EditUser user={user} key={user.id} />
        ))}
      </div>
    </>
  );
};

export { RenderUsers };
