import React, { FC, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { EditUsersStyle } from './styles/EditUsersStyle';

import { RootState } from '../../state/store';

import { Header } from './Header';
import { RenderUsers } from './RenderUsers';

const EditUsers: FC = () => {
  const { user } = useSelector((store: RootState) => store.user);

  const navigate = useNavigate();

  useEffect(() => {
    if (!user) {
      navigate(`/login`);
    }
  });

  return (
    <div>
      <Header />
      <EditUsersStyle>
        <RenderUsers />
      </EditUsersStyle>
    </div>
  );
};

export { EditUsers };
