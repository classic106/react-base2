import React, { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { HeaderStyle } from './styles/Header';

import { store, RootState } from '../../state/store';
import { getMessages } from '../../state/messagesReducer';
import { logOutUser } from '../../helpers';

const Header: FC = () => {
  const { user } = useSelector((store: RootState) => store.user);
  const navigate = useNavigate();

  const onLogOut = () => logOutUser();
  const onToChat = () => {
    if (user) {
      store.dispatch(getMessages(user.id));
      navigate(`/`);
    }
  };

  return (
    <HeaderStyle>
      <button onClick={onToChat}>To Chat</button>
      <button onClick={onLogOut}>LOGOUT</button>
    </HeaderStyle>
  );
};

export { Header };
