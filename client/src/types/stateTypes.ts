import { IUser } from './index';

interface IUserState {
  user: IUser | null;
  logError: string | null;
}

interface IAddUser {
  user: {
    name: string;
    email: string;
    isAdmin?: boolean;
  };
  currentUserId: string;
}

export type { IAddUser, IUserState };
