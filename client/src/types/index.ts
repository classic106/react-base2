interface IMessage {
  avatar: string;
  createdAt: string;
  editedAt: string;
  id: string;
  text: string;
  user: string;
  userId: string;
}

interface IUser {
  id: string;
  name: string;
  email: string;
  isAdmin?: boolean;
}

interface IRedactMessage {
  messageId: string;
  text: string;
  edit?: boolean;
}

interface IAction {
  type: string;
  payload: string;
}

export type { IMessage, IUser, IRedactMessage, IAction };
